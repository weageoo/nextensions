﻿using System;
using System.Threading;
using NUnit.Framework;
using System.NExtensions;

// ReSharper disable InconsistentNaming
// ReSharper disable AccessToDisposedClosure
namespace NExtensions.Tests.System
{
    [TestFixture]
    public class ActionExtensionsTests
    {
        [Test]
        public void Action_Execute_After_TimeSpan_Test()
        {
            const int EXECUTE_AFTER_SPAN_MSEC = 1000;
            const int ALLOWLABLE_TIME_LAG_MSEC = 500;

            using (var mres = new ManualResetEventSlim())
            {
                //+ Arrange
                var action = new Action(() =>
                {
                    lock (mres) mres.Set();
                });
                var timeSpan = TimeSpan.FromMilliseconds(EXECUTE_AFTER_SPAN_MSEC);

                //+ Act
                action.ExecuteAfter(timeSpan, false);

                //+ Assert
                Assert.True(mres.Wait(EXECUTE_AFTER_SPAN_MSEC + ALLOWLABLE_TIME_LAG_MSEC));
            }
        }
    }
}