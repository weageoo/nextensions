﻿// ReSharper disable RedundantUsingDirective
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

// ReSharper disable once CheckNamespace
namespace System.NExtensions
{
    // ReSharper disable RedundantNameQualifier
    public static class ActionExtensions
    {
#if NET45
        public static void ExecuteAfter(this Action action,
            TimeSpan afterTimeout, bool fromCurrentSynchronizationContext = true)
        {
            if (fromCurrentSynchronizationContext)
            {
                Task.Delay(afterTimeout)
                    .ContinueWith(task => action(),
                        CancellationToken.None, TaskContinuationOptions.None,
                        TaskScheduler.FromCurrentSynchronizationContext());
            }
            else
            {
                Task.Delay(afterTimeout)
                    .ContinueWith(task => action());
            }
        }
#else
        public static void ExecuteAfter(this Action action,
            TimeSpan timeout, bool fromCurrentSynchronizationContext = true)
        {
            if (fromCurrentSynchronizationContext)
            {
                ExecuteAfter(action, timeout, Dispatcher.CurrentDispatcher);
            }
            else
            {
                ExecuteFromThreadPoolThread(action, timeout);
            }
        }
#endif

        public static void ExecuteAfter(this Action action,
            TimeSpan timeout, Dispatcher dispatcher, DispatcherPriority priority = DispatcherPriority.Background)
        {
            var timer = new DispatcherTimer(priority, dispatcher) { Interval = timeout };
            timer.Tick += (sender, args) =>
            {
                var tmr = sender as DispatcherTimer;
                if (tmr != null)
                {
                    tmr.Stop();
                }

                action();
            };
            timer.Start();
        }

        #region Utils

        private static void ExecuteFromThreadPoolThread(Action action, TimeSpan afterTimeout)
        {
            var holder = new InstanceHolder<System.Threading.Timer>();
            holder.Instance = new System.Threading.Timer(obj =>
            {
                var hldr = obj as InstanceHolder<System.Threading.Timer>;
                if (hldr != null)
                {
                    var tmr = hldr.Instance;
                    if (tmr != null)
                    {
                        tmr.Dispose();
                    }
                }

                action();
            }, holder, afterTimeout,
            TimeSpan.FromMilliseconds(-1)); // disable periodic signaling (http://msdn.microsoft.com/en-GB/library/ah1h85ch.aspx)

            #region Alternative implementation

            //var timer = new System.Timers.Timer(afterTimeout.TotalMilliseconds)
            //{
            //    AutoReset = false
            //};
            //timer.Elapsed += (sender, args) =>
            //{
            //    var tmr = sender as System.Timers.Timer;
            //    if (tmr != null)
            //    {
            //        tmr.Dispose();
            //    }

            //    action();
            //};
            //timer.Start();

            #endregion
        }

        #region Nested

        private sealed class InstanceHolder<T>
        {
            public T Instance { get; set; }
        }

        #endregion

        #endregion
    }
}